package com.pixelcrunch.spov.app;

import android.app.Application;
import com.facebook.model.GraphUser;
import com.parse.Parse;
import com.parse.ParseObject;
import com.pixelcrunch.spov.app.parseObjects.Post;

/**
 * Created by Sean on 5/15/2014.
 */
public class SPOVApplication extends Application {

    public GraphUser graphUser;

    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Post.class);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "u4kM6INVOyb3fQAHp9J8boKUYMkLERt373XNVLcf", "dY2F52S1l1u4iMruX5jJDNu0PBdeZzhce5kDJOoS");
        //ParseFacebookUtils.initialize(getString(R.string.app_id));

        /*
         * This app lets an anonymous user create and save photos of meals
		 * they've eaten. An anonymous user is a user that can be created
		 * without a username and password but still has all of the same
		 * capabilities as any other ParseUser.
		 *
		 * After logging out, an anonymous user is abandoned, and its data is no
		 * longer accessible. In your own app, you can convert anonymous users
		 * to regular users so that data persists.
		 *
		 * Learn more about the ParseUser class:
		 * https://www.parse.com/docs/android_guide#users
		 */

        //ParseUser.enableAutomaticUser();
    }



}
