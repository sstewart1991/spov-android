package com.pixelcrunch.spov.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.*;
import com.pixelcrunch.spov.app.Utils.GPSTracker;
import com.pixelcrunch.spov.app.parseObjects.Post;

public class UploadActivity extends Activity {
    final private static String CAMERA_FRAGMENT = "CAMERA_FRAGMENT";
    final private static String TAG = "UploadActivity";

    byte[] holdImageArray;

    private Post mNewPost;
    private FragmentManager mFragmentManager;

    private GPSTracker mBoundGPSTracker;
    private double mLatitude;
    private double mLongitude;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            Log.d(TAG, "Service Connected");
            mBoundGPSTracker = ((GPSTracker.LocalBinder) service).getService();
            mIsBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            Log.d(TAG, "Service Disconnected");
            mBoundGPSTracker = null;
            mIsBound = false;
        }
    };
    private boolean mIsBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mNewPost = new Post();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        mFragmentManager = getFragmentManager();
        Fragment fragment = mFragmentManager.findFragmentById(R.id.uploadFragmentContainer);

        if (fragment == null) {
            fragment = new CameraFragment();
            mFragmentManager.beginTransaction().add(R.id.uploadFragmentContainer, fragment, CAMERA_FRAGMENT).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        doBindService();
    }

    @Override
    protected void onStop() {
        super.onStop();

        doUnbindService();
    }

    private void doBindService() {
        Log.d(TAG, "Called doBindService");
        bindService(new Intent(UploadActivity.this, GPSTracker.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    public void doUnbindService() {
        Log.d(TAG, "Called doUnbindService");

        if (mIsBound) {
            Log.d(TAG, "Calling unbindService");
            unbindService(mConnection);
            mBoundGPSTracker = null;
            mIsBound = false;
        }
    }

    public void setHoldImageArray(byte[] imageByteArray) {
        holdImageArray = imageByteArray;
    }

    public byte[] getHoldImageArray() {
        return holdImageArray;
    }

    public void retrieveCoordinates() {
        mLatitude = mBoundGPSTracker.getLatitude();
        mLongitude = mBoundGPSTracker.getLongitude();
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.upload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Cannot access onKeyDown from fragment,
        // Instead we have to tell the activity to
        // call a custom function on the CameraFragment
        // if the hardware camera button is pressed
        if (keyCode == KeyEvent.KEYCODE_CAMERA) {
            try {
                ((CameraFragment) mFragmentManager.findFragmentByTag(CAMERA_FRAGMENT)).myCameraKeyDown();
                return true;
            } catch (NullPointerException e) {
                // CameraFragment is not active, ignore
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}
