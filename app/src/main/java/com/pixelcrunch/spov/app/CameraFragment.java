package com.pixelcrunch.spov.app;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.*;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.parse.ParseFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Sean on 5/15/2014.
 */
public class CameraFragment extends Fragment implements SensorEventListener {

    public static final String TAG = "CameraFragment";

    private enum Orientation {
        PORTRAIT, LANDSCAPE, PORTRAIT_180, LANDSCAPE_180
    }

    private SensorManager sensorManager;
    private long lastUpdate;

    private Camera mCamera;
    private SurfaceView mCameraPreview;
    private ImageView mImagePreview;
    private ParseFile mPhotoFile;

    private ImageButton mCameraButton;
    private LinearLayout mConfirmButtonHolder;
    private ImageButton mCancelButton;
    private ImageButton mAcceptButton;

    private Orientation mOrientation = Orientation.PORTRAIT;

    // Make sure we don't take a picture
    // if camera isn't available
    private boolean isCameraAvailable = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_camera, parent, false);

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        lastUpdate = System.currentTimeMillis();

        mCameraButton = (ImageButton) v.findViewById(R.id.uploadCameraButton);

        mCameraPreview = (SurfaceView) v.findViewById(R.id.uploadCameraSurfaceView);
        SurfaceHolder holder = mCameraPreview.getHolder();

        // TODO Handle error if holder is null
        if (holder != null) {
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        if (mCamera != null) {
                            mCamera.setDisplayOrientation(90);
                            mCamera.setPreviewDisplay(holder);
                            mCamera.startPreview();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Error setting up preview", e);
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                    // nothing to do here
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    // nothing to do here
                }
            });
        }


        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePictureIfPossible();
            }
        });

        mImagePreview = (ImageView) v.findViewById(R.id.uploadCameraImagePreview);
        mConfirmButtonHolder = (LinearLayout) v.findViewById(R.id.uploadConfirmButtonHolder);
        mCancelButton = (ImageButton) v.findViewById(R.id.uploadCameraCancelButton);
        mAcceptButton = (ImageButton) v.findViewById(R.id.uploadCameraAcceptButton);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        enableCamera();

        // Hack to fix camera preview freeze on phone unlock
        mCameraPreview.setVisibility(View.VISIBLE);

        // register this class as a listener for the orientation and
        // accelerometer sensors
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onPause() {
        sensorManager.unregisterListener(this);

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            isCameraAvailable = false;
            mCameraButton.setEnabled(false);
        }

        // Hack to fix camera preview freeze on phone unlock
        mCameraPreview.setVisibility(View.GONE);

        super.onPause();
    }

    private void enableCamera() {
        // TODO Setup camera focus features - http://developer.android.com/guide/topics/media/camera.html
        if (mCamera == null) {
            try {
                mCamera = Camera.open();
                mCameraButton.setEnabled(true);
                isCameraAvailable = true;
                // get Camera parameters
                Camera.Parameters params = mCamera.getParameters();
                // set the focus mode
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                // set Camera parameters
                mCamera.setParameters(params);
            } catch (Exception e) {
                Log.e(TAG, "No camera with exception: " + e.getMessage());
                mCameraButton.setEnabled(false);
                Toast.makeText(getActivity(), "No camera detected", Toast.LENGTH_LONG).show();
                // TODO Show gallery if no camera is present
            }
        }
    }

    /**
     * ParseQueryAdapter loads ParseFiles into a ParseImageView at whatever size
     * they are saved. Since we never need a full-size image in our app, we'll
     * save a scaled one right away.
     */
    private void generateAndSendScaledPhoto(Bitmap photoBitmap) {

        // Resize photo from camera byte array
        Bitmap postImageScaled = Bitmap.createScaledBitmap(photoBitmap, 1280,
                1280 * photoBitmap.getHeight() / photoBitmap.getWidth(), false);

        // TODO Save photo to external storage

        // Compress into JPEG format at 80% quality
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        postImageScaled.compress(Bitmap.CompressFormat.JPEG, 75, byteOutputStream);

        // Final scaled and compressed JPEG image
        byte[] scaledPhoto = byteOutputStream.toByteArray();

        // Send Image to Activity to hold
        ((UploadActivity)getActivity()).setHoldImageArray(scaledPhoto);

        // Go to NewPostFragment
        Fragment newPostFragment = new NewPostFragment();
        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
        transaction.replace(R.id.uploadFragmentContainer, newPostFragment);
        transaction.addToBackStack("NewPostFragment");
        transaction.commit();

    }

    private void takePictureIfPossible() {
        if (mCamera == null) {
            return;
        }
        mCamera.takePicture(new Camera.ShutterCallback() {
            @Override
            public void onShutter() {
                isCameraAvailable = false;
            }
        }, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                confirmPhoto(data);
            }
        });
    }

    private void confirmPhoto(byte[] data) {
        Bitmap postImage = BitmapFactory.decodeByteArray(data, 0, data.length);

        // Override Android default landscape orientation if picture in portrait
        if (mOrientation == Orientation.PORTRAIT) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            postImage = Bitmap.createBitmap(postImage, 0, 0, postImage.getWidth(), postImage.getHeight(), matrix, true);
        } else if (mOrientation == Orientation.PORTRAIT_180) {
            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            postImage = Bitmap.createBitmap(postImage, 0, 0, postImage.getWidth(), postImage.getHeight(), matrix, true);
        } else if (mOrientation == Orientation.LANDSCAPE_180) {
            Matrix matrix = new Matrix();
            matrix.postRotate(180);
            postImage = Bitmap.createBitmap(postImage, 0, 0, postImage.getWidth(), postImage.getHeight(), matrix, true);
        }

        mImagePreview.setVisibility(View.VISIBLE);
        mImagePreview.setImageBitmap(postImage);
        mCameraPreview.setVisibility(View.GONE);
        mCameraButton.setVisibility(View.GONE);
        mConfirmButtonHolder.setVisibility(View.VISIBLE);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImagePreview.setVisibility(View.GONE);
                mCameraPreview.setVisibility(View.VISIBLE);
                mCameraButton.setVisibility(View.VISIBLE);
                mConfirmButtonHolder.setVisibility(View.GONE);
                isCameraAvailable = true;
            }
        });

        final Bitmap finalPostImage = postImage;
        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateAndSendScaledPhoto(finalPostImage);
            }
        });
    }

    /**
     * This function is called from the Activity of this function.
     * The activity should call this when the hardware camera key
     * is pressed on the mobile device.
     */
    public void myCameraKeyDown() {
        if (isCameraAvailable) {
            takePictureIfPossible();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
            getAccelerometer(event);
        }
    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];


        long actualTime = System.currentTimeMillis();

        if (actualTime - lastUpdate < 200) {
            return;
        }
        lastUpdate = actualTime;

        if (mOrientation == Orientation.PORTRAIT) {
            if (x > 7) {
                mOrientation = Orientation.LANDSCAPE;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 0, 90).start();
            }else if (x < -7) {
                mOrientation = Orientation.LANDSCAPE_180;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 0, -90).start();
            }
        } else if (mOrientation == Orientation.LANDSCAPE) {
            if (y > 7) {
                mOrientation = Orientation.PORTRAIT;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 90, 0).start();
            } else if (y < -7){
                mOrientation = Orientation.PORTRAIT_180;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 90, 180).start();
            }
        } else if (mOrientation == Orientation.PORTRAIT_180) {
            if (x > 7) {
                mOrientation = Orientation.LANDSCAPE;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 180, 90).start();
            }else if (x < -7) {
                mOrientation = Orientation.LANDSCAPE_180;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 180, 270).start();
            }
        } else if (mOrientation == Orientation.LANDSCAPE_180) {
            if (y > 7) {
                mOrientation = Orientation.PORTRAIT;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 270, 360).start();
            } else if (y < -7){
                mOrientation = Orientation.PORTRAIT_180;
                ObjectAnimator.ofFloat(mCameraButton, "rotation", 270, 180).start();
            }
        }
        //Log.d(TAG, x + " " + y + " " + z);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
