package com.pixelcrunch.spov.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.parse.*;
import com.pixelcrunch.spov.app.parseObjects.Post;
import com.pixelcrunch.spov.app.Utils.MultiDrawable;
import org.joda.time.Instant;

import java.util.ArrayList;
import java.util.List;


/**
 * Creates a MapFragment that is displayed to the user.
 */
public class MainMapFragment extends Fragment implements ClusterManager.OnClusterClickListener<Post>, ClusterManager.OnClusterInfoWindowClickListener<Post>, ClusterManager.OnClusterItemClickListener<Post>, ClusterManager.OnClusterItemInfoWindowClickListener<Post>{
    private static final String TAG = "MainMapFragment";

    private static final String SPOV_PREFS = "com.pixelcrunch.spov";
    private static final String PREFS_LAST_UPDATE = "lastUpdate";
    private static final long MIN_UPDATE_INTERVAL = 15 * 60 * 1000; // 15 Minutes
    private SharedPreferences mPrefs;

    private LinearLayout mImageGallery;
    private GoogleMap mMap;
    private ClusterManager<Post> mClusterManager;

    private List<Post> mPosts;
    private static int mPostCount = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mapFragment = inflater.inflate(R.layout.fragment_main_map, container, false);

        mPrefs = getActivity().getSharedPreferences(SPOV_PREFS, Activity.MODE_PRIVATE);

        mImageGallery = (LinearLayout) mapFragment.findViewById(R.id.imageGallery);
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        mMap.setMyLocationEnabled(true);

        mClusterManager = new ClusterManager<Post>(getActivity(), mMap);
        mClusterManager.setRenderer(new PostRenderer());
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        return mapFragment;

    }

    @Override
    public void onResume() {
        super.onResume();

        updateMapFromCache();
        getNewPosts();
    }

    private void getNewPosts() {
        //TODO app doesn't delete posts from cache if deleted on Parse
        //TODO clean up cache when not using posts anymore
        // If null user hasn't run app before. Set to arbitrary old date.
        String lastUpdateString = mPrefs.getString(PREFS_LAST_UPDATE, "2000-01-01T12:00:00.000+0000");
        Instant lastUpdate = Instant.parse(lastUpdateString);

        // Minimum update interval minutes has not passed yet.
        if (new Instant().isBefore(lastUpdate.plus(MIN_UPDATE_INTERVAL))) {
            return;
        }

        // http://blog.parse.com/2013/05/30/parse-on-android-just-got-classier/

        ParseQuery<Post> query = ParseQuery.getQuery(Post.class);
        // TODO Only get objects we don't have cached
        //query.whereGreaterThan()
        //query.setLimit(10); // limit to at most 10 results
        //query.setSkip(10); // skip the first 10 results

        // Show newest updated posts
        // Moderator could change comment attached to Post. We must update cache to reflect this change
        query.orderByDescending("updatedAt");

        // Gets all posts from Parse.
        query.findInBackground(new FindCallback<Post>() {
            @Override
            public void done(final List<Post> posts, ParseException e) {
                if (e != null) {
                    // There was an error or the network wasn't available
                    // TODO Display error message. Parse has already retried multiple times.
                    return;
                }

                // Nothing to update
                if (posts.size() == 0) {
                    return;
                }

                // Add the latest Posts from this query to the local cache
                // Question asked at: http://stackoverflow.com/questions/23962324/parse-for-android-cant-pin-list-of-subclass-to-cache
                //Post.pinAllInBackground(POST_CACHE_LABEL, posts);
                for (Post aPost : posts) {
                    aPost.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                        }
                    });
                }

                updateMapFromCache();
            }
        });
    }

    private void updateMapFromCache() {
        final Bitmap.Config conf = Bitmap.Config.ARGB_8888;

        //mMap.clear();
        mClusterManager.clearItems();// Remove all markers from map

        ParseQuery<Post> query = ParseQuery.getQuery(Post.class);
        query.fromLocalDatastore(); // Query from local cache
        // Show newest updated posts
        query.orderByDescending("updatedAt");
        query.findInBackground(new FindCallback<Post>() {
            @Override
            public void done(List<Post> parseObjects, ParseException e) {
                if (e != null) {
                    // First time app is run there is no cache available.
                    return;
                }

                mPosts = parseObjects;


                for (Post aPost : mPosts) {
                    final double latitude = aPost.getLatitude();
                    final double longitude = aPost.getLongitude();

                    ParseFile thumbnail = aPost.getThumbnail();

                    // No thumbnail available for this post. Ignore it and move to next post.
                    if (thumbnail == null) {
                        continue;
                    }

                    final Post thePost = aPost;

                    thumbnail.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] photo, ParseException e) {
                            if (e != null) {
                                Log.e(TAG, "Thumbnail retrieve error: " + e);
                                return;
                            }

                            Bitmap postImage = BitmapFactory.decodeByteArray(photo, 0, photo.length);
                            thePost.thumbnail = postImage;

                            mClusterManager.addItem(thePost);

                            mPostCount++;

                            // Cluster once we've added all the items
                            if (mPostCount == mPosts.size()) {
                                mClusterManager.cluster();
                                mPostCount = 0;
                            }
                        }
                    });


                }
            }
        });
    }

    @Override
    public boolean onClusterClick(Cluster<Post> postCluster) {
        // Show a toast with some info when the cluster is clicked.
        //String details = postCluster.getItems().iterator().next().getComment();
        //Toast.makeText(getActivity(), postCluster.getSize() + " (including " + details + ")", Toast.LENGTH_SHORT).show();

        mImageGallery.removeAllViews();
        List<Post> clusterPosts = new ArrayList<Post>(postCluster.getItems());
        for(Post post : clusterPosts){
            mImageGallery.addView(insertPhoto(post));
        }

        // Save this list in the MainActivity so the Gallery can access it
        ((MainActivity)getActivity()).setSelectedPosts(clusterPosts);
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Post> postCluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(Post post) {
        ((MainActivity)getActivity()).setSelectedPost(post);

        // Save Post to MainActivity so ImageGallery can access it
        List<Post> posts = new ArrayList<Post>();
        posts.add(post);
        ((MainActivity)getActivity()).setSelectedPosts(posts);

        goToImageGallery();
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Post post) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    /**
     *
     * Places the post Thumbnail into mImageGallery, this will make mImageGallery visible.
     *
     * Also set up the onClickListener for the thumbnail in mImageGallery so that it goes to the ImageGallery
     *
     * @param post
     * @return
     */
    View insertPhoto(final Post post){
        LinearLayout layout = new LinearLayout(getActivity().getApplicationContext());
        layout.setLayoutParams(new LinearLayout.LayoutParams(250, 250));
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getActivity().getApplicationContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(220, 220));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(post.thumbnail);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mImageGallery.removeAllViews();

                ((MainActivity)getActivity()).setSelectedPost(post);

                goToImageGallery();
            }
        });

        layout.addView(imageView);
        return layout;
    }

    /**
     *  Instantiate and then put the ImageGalleryFragment in-front of the MapFragment.
     *  Pressing the back button from the ImageGalleryFragment will destroy the fragment making the MapFragment visible again
     */
    private void goToImageGallery(){
        Fragment imageGalleryFragment = new ImageGalleryFragment();
        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainActivityContainer, imageGalleryFragment);
        transaction.addToBackStack("ImageGalleryFragment");
        transaction.commit();
    }

    private class PostRenderer extends DefaultClusterRenderer<Post> {

        private final IconGenerator mIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public PostRenderer() {
            super(getActivity().getApplicationContext(), mMap, mClusterManager);


            View multiPost = getActivity().getLayoutInflater().inflate(R.layout.marker_multi_post, null);
            mClusterIconGenerator.setContentView(multiPost);
            mClusterImageView = (ImageView) multiPost.findViewById(R.id.postThumbnail);

            mImageView = new ImageView(getActivity().getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_post_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_post_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(Post post, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            mImageView.setImageBitmap(post.thumbnail);

            int height = post.thumbnail.getHeight();
            int width = post.thumbnail.getWidth();
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
            mIconGenerator.setContentView(mImageView);

            Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Post> cluster, MarkerOptions markerOptions) {
            //Draw multiple people
            // Note: this method runs on the UI thread. Don't spend too much time in here.
            List<Drawable> postPhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Post post : cluster.getItems()){
                // Draw 4 at most
                if (postPhotos.size() == 4){
                    break;
                }
                Drawable drawable = new BitmapDrawable(getResources(),post.thumbnail);
                drawable.setBounds(0, 0, width, height);
                postPhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(postPhotos);
            multiDrawable.setBounds(0, 0 , width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster){
            // always render clusters.
            return cluster.getSize() > 1;
        }
    }

}
