package com.pixelcrunch.spov.app;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.pixelcrunch.spov.app.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CameraIntentFragment extends Fragment {
    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final String FILE_URI_KEY = "file_uri";

    // directory name to store captured images
    private static final String IMAGE_DIRECTORY_NAME = "SPOV";

    // file url to store image
    private Uri mFileUri;

    private ImageView mImgPreview;
    private ImageButton mBtnCancel, mBtnAccept;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_camera_intent, container, false);

        init(v);

        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getActivity().getApplicationContext(), "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }

        captureImage();

        // Return the view
        return v;
    }

    private void init(View v) {
        mImgPreview = (ImageView) v.findViewById(R.id.uploadCameraIntentImagePreview);
        mBtnAccept = (ImageButton) v.findViewById(R.id.uploadIntentCameraAcceptButton);
        mBtnCancel = (ImageButton) v.findViewById(R.id.uploadIntentCameraCancelButton);

        mBtnAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                captureImage();
            }
        });
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // Device has camera
            return true;
        } else {
            // Device has no camera
            return false;
        }
    }

    /**
     * Launch camera app, requesting image
     */
    private void captureImage() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        mFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);

        // start the image capture Intent
        startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Here we store the file url as it will be null after returning
     * from the camera app
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save file url in bundle as it will be null on screen orientation change
        outState.putParcelable(FILE_URI_KEY, mFileUri);
    }

    // Same as onRestoreInstanceState but used in Fragments
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // get the file url
        mFileUri = savedInstanceState.getParcelable(FILE_URI_KEY);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // user cancelled image Capture
                Toast.makeText(getActivity().getApplicationContext(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                Toast.makeText(getActivity().getApplicationContext(), "Failed to capture image", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Display iamge from a path to ImageView
     */
    private void previewCapturedImage() {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for lager images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(mFileUri.getPath(), options);

            mImgPreview.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * -------------Helper Methods-------------
     */

    /**
     * Creating file uri to store image
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Returning image
     */
    private static File getOutputMediaFile(int type) {
        // External sd card location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Could not create " + IMAGE_DIRECTORY_NAME + "directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyymMMdd+HHmmss", Locale.getDefault()).format(new Date());

        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG+" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }
}
