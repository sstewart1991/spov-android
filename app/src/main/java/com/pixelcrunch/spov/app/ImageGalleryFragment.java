package com.pixelcrunch.spov.app;

import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pixelcrunch.spov.app.parseObjects.Post;
import com.pixelcrunch.spov.app.Utils.ImageViewTouchViewPager;
import com.squareup.picasso.Picasso;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;

import java.util.List;

/**
 * Created by Sean on 6/19/2014.
 */
public class ImageGalleryFragment extends Fragment {
    private static final String TAG = "ImageGalleryFragment";

    private List<Post> mPosts;
    private Post mSelectedPost;

    ImageViewTouchViewPager mPager;
    TextView mPostComment;
    ImagePagerAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_gallery, container, false);
        mPosts = ((MainActivity)getActivity()).getSelectedPosts();
        mSelectedPost = ((MainActivity)getActivity()).getSelectedPost();

        mPager = (ImageViewTouchViewPager) view.findViewById(R.id.imageViewPager);
        mPager.setImageGalleryFragment(this); // Give reference back to this object so we can set comment later

        mPostComment = (TextView) view.findViewById(R.id.imageGalleryPostComment);
        mPostComment.setText(mSelectedPost.getComment());

        mAdapter = new ImagePagerAdapter();
        mPager.setAdapter(mAdapter);

        int pagerPosition = mPosts.indexOf(mSelectedPost);
        mPager.setCurrentItem(pagerPosition, true);

        return view;
    }

    public void setPostComment(int position) {
        mPostComment.setText(mPosts.get(position).getComment());
    }

    /**
     * Adapter to display the Images grabbed from MainActivity.
     * These should be the Images from the Cluster that they clicked on in the map
     *
     * We use Picasso to deal with caching of images. Picasso uses the thumbnail till the full Image is loaded from the Parse URL
     */
    class ImagePagerAdapter extends PagerAdapter
    {
        @Override
        public int getCount()
        {
            return mPosts.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            Context context = container.getContext();

            ImageViewTouch imageView = new ImageViewTouch(context);
            imageView.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);

            Post currentPost = mPosts.get(position);

            Picasso.with(context)
                    .load(currentPost.getPhoto().getUrl())
                    .placeholder(new BitmapDrawable(getResources(),currentPost.thumbnail))
                    .error(R.drawable.ic_launcher)
                    .into(imageView);
            container.addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object o)
        {
            return view == o;
        }

        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_NONE;
        }

    }
}
