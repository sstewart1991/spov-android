package com.pixelcrunch.spov.app;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;
import com.pixelcrunch.spov.app.parseObjects.Post;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by Sean on 5/15/2014.
 */
public class NewPostFragment extends Fragment {
    private static final String TAG = "NewPostFragment";

    private byte[] mPhotoByteArray;
    private Bitmap mPostPhoto;

    private ImageView mPhotoView;
    private EditText mDescription;
    private Button mCancel;
    private Button mPost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_new_post, parent, false);

        mPhotoByteArray = ((UploadActivity) getActivity()).getHoldImageArray();

        init(v);

        return v;
    }

    private void init(View v) {
        mPhotoView = (ImageView) v.findViewById(R.id.uploadPost_ivPhoto);
        mDescription = (EditText) v.findViewById(R.id.uploadPost_etDescription);
        mCancel = (Button) v.findViewById(R.id.uploadPost_btnCancel);
        mPost = (Button) v.findViewById(R.id.uploadPost_btnPost);

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postPhotoToParseAndFacebook();
            }
        });

        if (mPhotoByteArray == null) {
            // TODO Handle No Photo Error
            Log.e(TAG, "mPhotoByteArray was empty, cannot get image");
            return;
        }

        // Convert byteArray to Bitmap
        mPostPhoto = BitmapFactory.decodeByteArray(mPhotoByteArray, 0, mPhotoByteArray.length);

        mPhotoView.setImageBitmap(mPostPhoto);

    }

    private void postPhotoToParseAndFacebook() {
        Post newPost = new Post();

        ParseFile parsePhoto = new ParseFile("picture.jpg", mPhotoByteArray);
        newPost.setPhoto(parsePhoto);

        ParseFile parseThumbnail = new ParseFile("picture.jpg", generateThumbnail());
        newPost.setThumbnail(parseThumbnail);

        newPost.setComment(mDescription.getText().toString());
        //newPost.setAuthor(ParseUser.getCurrentUser());
        newPost.setPostDate(new Date());

        UploadActivity uploadActivity = ((UploadActivity) getActivity());
        uploadActivity.retrieveCoordinates();
        uploadActivity.doUnbindService();

        if (uploadActivity.getLatitude() != 0) {
            newPost.setLatitude(uploadActivity.getLatitude());
            newPost.setLongitude(uploadActivity.getLongitude());
        }

        newPost.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Toast.makeText(getActivity(),
                            "Error saving: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } else {
                    getActivity().finish();
                }
            }
        });

        // TODO Save to Facebook
    }

    private byte[] generateThumbnail() {

        Bitmap postImage = BitmapFactory.decodeByteArray(mPhotoByteArray, 0, mPhotoByteArray.length);

        //TODO Check if landscape or portrait
        // Resize photo from camera byte array
        Bitmap postImageScaled = Bitmap.createScaledBitmap(postImage, 192,
                192 * postImage.getHeight() / postImage.getWidth(), false);

        // TODO Save photo to external storage

        // Compress into JPEG format at 75% quality
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        postImageScaled.compress(Bitmap.CompressFormat.JPEG, 70, byteOutputStream);

        // Final scaled and compressed JPEG image
        return byteOutputStream.toByteArray();
    }


}
