package com.pixelcrunch.spov.app.Utils;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class GPSTracker extends Service implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    private static final String TAG = "GPSTracker";

    private boolean mCurrentlyProcessingLocation = false;
    private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;

    private double mLatitude;
    private double mLongitude;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        public GPSTracker getService() {
            return GPSTracker.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (!mCurrentlyProcessingLocation) {
            mCurrentlyProcessingLocation = true;
            startTracking();
        }
    }

    public void startTracking() {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            mLocationClient = new LocationClient(this, this, this);

            if (!mLocationClient.isConnected() || !mLocationClient.isConnecting()) {
                mLocationClient.connect();
            } else {
                Log.e(TAG, "Unable to connect to Google Play Services");
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            Log.d(TAG, "Location updated: " + mLatitude + " " + mLongitude);
            // Desired accuracy of 100 meters
            if (location.getAccuracy() < 100.0f) {
                Log.d(TAG, "Stop location Updates");
                stopLocationUpdates();
            }
        }
    }

    private void stopLocationUpdates() {
        if (mLocationClient != null && mLocationClient.isConnected()) {
            mLocationClient.removeLocationUpdates(this);
        }

        mLocationClient.disconnect();
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    /**
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000); // milliseconds
        mLocationRequest.setFastestInterval(1000); // the fastest rate in milliseconds at which your app can handle location
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    /**
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
