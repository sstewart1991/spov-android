package com.pixelcrunch.spov.app;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import java.util.Arrays;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SplashFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SplashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SplashFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View splashFragmentView = inflater.inflate(R.layout.fragment_main_splash, container, false);

        // TODO Request proper permissions needed for app - will request additional permissions when they try to post
        LoginButton authButton = (LoginButton) splashFragmentView.findViewById(R.id.SplashFragment_login_button);
        //authButton.setFragment(this); // Don't think this is needed. Forces SplashFragment to handle onSessionStateChange()
        authButton.setReadPermissions(Arrays.asList("public_profile"));

        return splashFragmentView;
    }

}
