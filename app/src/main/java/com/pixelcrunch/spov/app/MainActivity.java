package com.pixelcrunch.spov.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.facebook.*;
import com.facebook.model.GraphUser;
import com.pixelcrunch.spov.app.parseObjects.Post;

import java.util.List;

public class MainActivity extends Activity {
    private static final int SPLASH = 0;
    private static final int MAP = 1;
    private static final int FRAGMENT_COUNT = MAP + 1;

    private Fragment[] mFragments = new Fragment[FRAGMENT_COUNT];
    private boolean mIsResumed = false;
    private UiLifecycleHelper mUiHelper;
    private Session.StatusCallback mCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private List<Post> mSelectedPosts;
    private Post mSelectedPost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Used to help interface with Facebook SDK which handles user authentication for us
        // Pass a reference to onSessionStateChange() to be called later on session state changes (logging in/out)
        mUiHelper = new UiLifecycleHelper(this, mCallback);
        mUiHelper.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getFragmentManager();
        mFragments[SPLASH] = fm.findFragmentById(R.id.splashFragment);
        mFragments[MAP] = fm.findFragmentById(R.id.mapFragment);

        FragmentTransaction transaction = fm.beginTransaction();
        for (Fragment aFragment : mFragments) {
            transaction.hide(aFragment);
        }
        transaction.commit();
    }

    private void showFragment(int fragmentIndex, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        for (int i = 0; i < mFragments.length; i++) {
            if (i == fragmentIndex) {
                transaction.show(mFragments[i]);
            } else {
                transaction.hide(mFragments[i]);
            }
        }
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    /**
     * Called when authentication state changes, such as logging in or out of facebook account.
     */
    private void onSessionStateChange(Session session, SessionState state, Exception expression) {
        // Only make changes if the activity is visible
        if (mIsResumed) {
            FragmentManager manager = getFragmentManager();
            // Get the number of entries in the back stack
            int backStackSize = manager.getBackStackEntryCount();
            // Clear the back stack
            for (int i = 0; i < backStackSize; i++) {
                manager.popBackStack();
            }
            if (state.isOpened()) {
                // If the session state is open:
                // Show the authenticated fragment
                showFragment(MAP, false);
                makeMeRequest(session);
            } else if (state.isClosed()) {
                // If the session state is closed:
                // Show the login fragment
                showFragment(SPLASH, false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu;
        // this adds items to the action bar if the Map fragment is active.
        if (mFragments[MAP].isVisible()) {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_camera:
                Intent cameraIntent = new Intent(this, UploadActivity.class);
                startActivity(cameraIntent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // if the session is already open,
            // try to show the selection fragment
            showFragment(MAP, false);
        } else {
            // otherwise present the splash screen
            // and ask the person to login.
            showFragment(SPLASH, false);
        }

        mUiHelper.onResume();
        mIsResumed = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mUiHelper.onPause();
        mIsResumed = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mUiHelper.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUiHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mUiHelper.onSaveInstanceState(outState);
    }

    private void makeMeRequest(final Session session) {
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                // Set the Application user to the current user
                                ((SPOVApplication) getApplication()).graphUser = user;
                            }
                        }
                        if (response.getError() != null) {
                            // Handle errors, will do so later.
                        }
                    }
                });
        request.executeAsync();
    }

    public List<Post> getSelectedPosts() {
        if (mSelectedPosts == null) {
            return null; // TODO Should handle this better? Doesn't do anything currently
        }

        return mSelectedPosts;
    }

    public void setSelectedPosts(List<Post> selectedPosts) {
        mSelectedPosts = selectedPosts;
    }

    public Post getSelectedPost() {
        return mSelectedPost;
    }

    public void setSelectedPost(Post selectedPost) {
        mSelectedPost = selectedPost;
    }

}
